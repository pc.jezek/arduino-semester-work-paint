#include <Esplora.h>
#include <TFT.h>
#include <SPI.h>
#include <string.h>

#define UP    SWITCH_4
#define DOWN  SWITCH_1
#define LEFT  SWITCH_
#define RIGHT SWITCH_
#define MENU_DOWN SWITCH_1
#define MENU_UP   SWITCH_4
#define ENTER SWITCH_2
#define BACK  SWITCH_3


enum states { HOME_DRAW, HOME_OPTIONS, HOME_CREDITS, DRAW, DRAW_MENU, OPTIONS, CREDITS};
enum states STATE, NEXT_STATE;
int DRAWSTATE, NEXT_DRAWSTATE;
enum positions { POS1, POS2, POS3 };
enum drawPositions { DPOS1, DPOS2, DPOS3, DPOS4, DPOS5, DPOS6, CLR };
unsigned long long g_ms = 0,  g_ms2 = 0, g_msCur;
int xpos, ypos , g_xPOS = 0, g_lastX = 0, g_lastY = 0;
int BRUSH_SIZE = 1;
byte buttonFlag = 0;
bool drawInit = false;

bool buttonEvent(int button)
{
  switch(button)
  {
    case MENU_UP:
       if (Esplora.readButton(MENU_UP) == LOW) {
         buttonFlag |= 1;
       } else if (buttonFlag & 1) {
         buttonFlag ^= 1;
         return true;
       }
       break;
    case MENU_DOWN:
       if (Esplora.readButton(MENU_DOWN) == LOW){
         buttonFlag |= 2;
       } else if (buttonFlag & 2){
         buttonFlag ^= 2;
         return true;
       }
       break;
    case BACK:
       if (Esplora.readButton(BACK) == LOW){
         buttonFlag |= 4;
       } else if (buttonFlag & 4){
         buttonFlag ^= 4;
         return true;
       }
       break;
    case ENTER:
     if (Esplora.readButton(ENTER) == LOW){
       buttonFlag |= 8;
     } else if (buttonFlag & 8) {
       buttonFlag ^= 8;
       return true;
     }
  }
   return false;
}


void change_position(int position){
  int ypos;
    EsploraTFT.fill(255,100,150);
    EsploraTFT.stroke(255,100,150);
    EsploraTFT.rect(0,0, 20,EsploraTFT.height());
    EsploraTFT.setTextSize(1);
    EsploraTFT.noFill();
    for ( int i = 40; i <=80; i+=20)
      EsploraTFT.rect(20,i, EsploraTFT.width()-40,17);
    
    EsploraTFT.stroke(255,255,255);
  switch(position){
    case POS1 : ypos = 40; break;
    case POS2 : ypos = 60; break;
    case POS3: ypos = 80; break;
  }
   EsploraTFT.text(">", 5, ypos+5 );
   EsploraTFT.stroke(200,200,250);
   EsploraTFT.rect(20,ypos, EsploraTFT.width()-40,17);
}

void drawMenu_position(int position){
  int dypos;
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.noFill();
    for ( int i = 0; i <=90; i+= 18)
      EsploraTFT.rect( EsploraTFT.width() - 18 , 2 + i, 16,16);

    EsploraTFT.stroke(255,255,255);
  switch(position){
    case DPOS1: dypos = 0; break; 
    case DPOS2: dypos = 18; break; 
    case DPOS3: dypos = 36; break; 
    case DPOS4: dypos = 54; break; 
    case DPOS5: dypos = 72; break; 
    case DPOS6: dypos = 90; break;
    case CLR:
      for ( int i = 0; i <=90; i+= 18)
        EsploraTFT.rect( EsploraTFT.width() - 18 , 2 + i, 16,16); 
    return;
  }
   EsploraTFT.stroke(200,200,250);
   EsploraTFT.rect( EsploraTFT.width() - 18, 2+dypos, 16, 16);
}



void getColor ( int & r, int & g, int & b ){
 int sliderVal  =  Esplora.readSlider();
 int realVal = map ( sliderVal, 0, 1023, 767, 0 ); // 3* 255 
 if (realVal < 256){
   r = realVal;
   b = 255 - realVal;
   g = 0;
  }
 else if (realVal < 512 ){
   r = 511 - realVal;
   g = realVal - 256;
   b = 0;
 }
 else if ( realVal < 768 ){
   g = 767 - realVal;
   b = realVal - 512;
   r = 0;
 }
}

void readJoystick(){
 int x = Esplora.readJoystickX();
 int y = Esplora.readJoystickY();

 if ( x > 100  && xpos > BRUSH_SIZE)
   xpos--;
 if ( x < -100 && xpos < EsploraTFT.width() - BRUSH_SIZE - 30 )
   xpos++;
 if ( y > 100 && ypos < EsploraTFT.height() - BRUSH_SIZE - 10 )
   ypos++;
 if (y < -100 && ypos > BRUSH_SIZE )
   ypos--;
}

void display_menu(int position){
  drawInit = false; 
  xpos = EsploraTFT.width()/2; ypos = EsploraTFT.height()/2;
  EsploraTFT.background(255,100,150); EsploraTFT.stroke(255,255,255);
  EsploraTFT.setTextSize(2);
  EsploraTFT.text("PAINT", 50, 15);
  EsploraTFT.setTextSize(1);
  EsploraTFT.text("DRAW", 68, 45 );
  EsploraTFT.text("OPTIONS", 60, 65 );
  EsploraTFT.text("CREDITS", 60, 85 );
  EsploraTFT.line(30, 33, EsploraTFT.width()-30, 33);
  change_position(position);
}

void drawCursors(){
  static int lastXpos = 0, lastYpos = 0;
  EsploraTFT.fill(255,255,255);
  EsploraTFT.stroke(255,255,255);
  if ( millis () < g_msCur + 10 || (xpos == lastXpos && ypos == lastYpos ))
    return;
  lastXpos = xpos, lastYpos = ypos;
  g_msCur = millis();
  EsploraTFT.rect(0,EsploraTFT.height()-5, EsploraTFT.width()-25, 5);
  EsploraTFT.rect(EsploraTFT.width()-25, 0, 5, EsploraTFT.height());
  EsploraTFT.stroke(0,0,0);
  EsploraTFT.text("^", xpos, EsploraTFT.height()-5);
  EsploraTFT.text("<", EsploraTFT.width()-25, ypos);
}


void displayDraw (){
  int r = 0, g = 0, b = 0;
  if (! drawInit ){
    EsploraTFT.background (255,255,255);
    drawMenu();
    drawInit = true;
  }
  if ( millis() < g_ms + 20 ) return;
  getColor ( r, g, b );
  g_ms = millis();
  readJoystick();
  drawCursors();
  EsploraTFT.fill(r,g,b);
  EsploraTFT.stroke(0,0,0);
  EsploraTFT.rect( EsploraTFT.width() - 15, EsploraTFT.height() - 15, 10, 10 );
  EsploraTFT.stroke(r,g,b);
  if ( Esplora.readButton(DOWN) == LOW)
    EsploraTFT.circle(xpos, ypos, BRUSH_SIZE);
}

void displayDrawMenu(){
  Serial.println ( DRAWSTATE); 
  int r = 0, g = 0, b = 0;
  getColor(r,g,b);  
   switch (DRAWSTATE)
   {
      case 0:    
          if(buttonEvent(MENU_DOWN)) {
            drawMenu_position(DPOS2);  
            NEXT_DRAWSTATE = 1;   
          } else if (buttonEvent(ENTER)){
            if ( BRUSH_SIZE > 1 ) BRUSH_SIZE--;
          }
          break;
      case 1:   
          if (buttonEvent(MENU_DOWN))  {
            drawMenu_position(DPOS3);
            NEXT_DRAWSTATE = 2;
          } else if (buttonEvent(MENU_UP)) {
            drawMenu_position(DPOS1);             
            NEXT_DRAWSTATE = 0;
          } else if (buttonEvent(ENTER)) {
             if ( BRUSH_SIZE < 7 ) BRUSH_SIZE++;
          }
          break;
 
      case 2:         
         
          if (buttonEvent(MENU_DOWN)) {
            drawMenu_position(DPOS4);          
            NEXT_DRAWSTATE = 3;
          } else if (buttonEvent(MENU_UP)) {
            drawMenu_position(DPOS2);            
            NEXT_DRAWSTATE = 1;
          } else if (buttonEvent(ENTER)) {
            int x = xpos , y = ypos ;
            if ( x + (BRUSH_SIZE * 5) > EsploraTFT.width() - 30
                 ||  y + (BRUSH_SIZE * 5) > EsploraTFT.height()-5 ){
                 
                   Esplora.writeRGB(255, 0, 0 );
                   delay(5);
                   Esplora.writeRGB(0,0,0);
                   break;
            }     
            EsploraTFT.fill(r,g,b); EsploraTFT.stroke(r,g,b);            
            EsploraTFT.rect(x, y, (BRUSH_SIZE * 5),(BRUSH_SIZE * 5) ) ;
          }
          else{
            if ( millis() < g_ms + 20 ) return;
            g_ms = millis();
            EsploraTFT.fill(r,g,b); EsploraTFT.stroke(0,0,0);   
            EsploraTFT.rect( EsploraTFT.width() - 15, EsploraTFT.height() - 15, 10, 10 );
            readJoystick(); drawCursors(); 
            
          }
          break;
 
      case 3:                                
          if (buttonEvent(MENU_DOWN))  
          {
            drawMenu_position(DPOS5);           
            NEXT_DRAWSTATE = 4;
          }
          else if (buttonEvent(MENU_UP))        
          {
            drawMenu_position(DPOS3);           
            NEXT_DRAWSTATE = 2;
          }
          else if (buttonEvent(ENTER))      
          {
            
          }
          break;
      case 4:                   
          if (buttonEvent(MENU_DOWN))              
          {
            drawMenu_position(DPOS6);           
            NEXT_DRAWSTATE = 5;
          }
          else if (buttonEvent(MENU_UP))            
          {
            drawMenu_position(DPOS4);           
            NEXT_DRAWSTATE = 3;
          }
          else if (buttonEvent(ENTER))       
          {

          }
          break;
      case 5:                                 
          if (buttonEvent(MENU_UP))           
          {
            drawMenu_position(DPOS5);             
            NEXT_DRAWSTATE = 4;
          }
          else if (buttonEvent(ENTER))        
          {
            
          }
          break;
   }
   DRAWSTATE = NEXT_DRAWSTATE;
}

void drawMenu(){
  EsploraTFT.stroke(0,0,0);
  EsploraTFT.fill(0,0,0);
  EsploraTFT.circle( EsploraTFT.width() - 11, 10, 2);
  EsploraTFT.circle( EsploraTFT.width() - 11, 27, 4);
  EsploraTFT.noFill();
  
  EsploraTFT.rect(EsploraTFT.width()-20, 0, 20, EsploraTFT.height());
}

void draw_mic (){
  int mic = map(Esplora.readMicrophone(), 0, 1023, EsploraTFT.height(), 0);
  int ms = millis();
  if (g_xPOS > EsploraTFT.width()){
    g_xPOS = 0;
    EsploraTFT.background(255,100,150);
  }
  if (ms > g_ms + 8){
    EsploraTFT.stroke(255,255,255);
    EsploraTFT.line(g_xPOS, EsploraTFT.height(), g_xPOS, mic);  
    g_ms = millis();
    g_xPOS++;
  }
}

void displayCredits(){
  if (  millis() > g_ms + 2000 ){
    g_ms = millis();
    EsploraTFT.background(0,0,0);
    EsploraTFT.text("Autor: Radek Jezek", 40, 30);
  }
}

void setup() {
 // Proveďte inicializaci displeje
  EsploraTFT.begin();
  EsploraTFT.background(255,255,255);
  Serial.begin(9600);
  display_menu(POS1);                          // zobrazení domovské obrazovky
  STATE = HOME_DRAW;
}

void loop()
{
  switch (STATE)
   {
      case HOME_DRAW:                       // MENU - TEMPERATURE
          if(buttonEvent(MENU_DOWN))                 // je stisknuto tlačítko 'pohyb dolu'?
          {
            change_position(POS2);            // změna domovské obrazovky
            NEXT_STATE = HOME_OPTIONS;
          }
          else if (buttonEvent(ENTER))          // je stisknuto tlačítko 'vybrat senzor'?
          {
            NEXT_STATE = DRAW;
          }
          break;
 
      case HOME_OPTIONS:                        // MENU - MICROPHONE
          if (buttonEvent(MENU_DOWN))                // je stisknuto tlačítko 'pohyb dolu'?
          {
            change_position(POS3);            // změna domovské obrazovky
            NEXT_STATE = HOME_CREDITS;
          }
          else if (buttonEvent(MENU_UP))             // je stisknuto tlačítko 'pohyb nahoru'?
          {
            change_position(POS1);             // změna domovské obrazovky
            NEXT_STATE = HOME_DRAW;
          }
          else if (buttonEvent(ENTER))          // je stisknuto tlačítko 'vybrat senzor'?
          {
            NEXT_STATE = OPTIONS;
          }
          break;
 
      case HOME_CREDITS:                       // MENU - ACCELEROMETER
          if (buttonEvent(MENU_UP))                  // je stisknuto tlačítko 'pohyb nahoru'?
          {
            change_position(POS2);             // změna domovské obrazovky
            NEXT_STATE = HOME_OPTIONS;
          }
          else if (buttonEvent(ENTER))         // je stisknuto tlačítko 'vybrat senzor'?
          {
            NEXT_STATE = CREDITS;
          }
          break;
 
      case DRAW:                                // DRAW TEMPERATURE
          if(buttonEvent(BACK))                 // je stisknuto tlačítko 'zpět do menu'?
          {
            display_menu(POS1);                 // zobrazení domovské obrazovky
            NEXT_STATE = HOME_DRAW;
          }
          else if (buttonEvent(ENTER)){
            displayDrawMenu();
            drawMenu_position(DPOS1);
            NEXT_STATE = DRAW_MENU;
          }
          else
          {
            displayDraw ();                        // funkce draw_temp vykreslí aktuální teplotu každé 2 sekundy
          }
          break;

      case DRAW_MENU:
          if (buttonEvent(BACK))
            {
              displayDraw();
              DRAWSTATE = 0;
              NEXT_DRAWSTATE = 0;
              NEXT_STATE = DRAW;
              drawMenu_position(CLR);
            }
          else
            {
             displayDrawMenu();
            } 
          break;
 
      case OPTIONS:                                 // DRAW MICROPHONE
          if(buttonEvent(BACK))                  // je stisknuto tlačítko 'zpět do menu'?
          {
            display_menu(POS2);                  // zobrazení domovské obrazovky
            NEXT_STATE = HOME_OPTIONS;
          }
          else
          {
            draw_mic();                        // funkce draw_mic vykreslí křivku podle momentálního zvuku z mikrofonu
          }
          break;
 
      case CREDITS:                                //  DRAW ACCELEROMETER
          if(buttonEvent(BACK))                 // je stisknuto tlačítko 'zpět do menu'?
          {
            display_menu(POS3);                 // zobrazení domovské obrazovky
            NEXT_STATE = HOME_CREDITS;
          }
          else
          {
            displayCredits();                       
          }
          break;
   }
   STATE = NEXT_STATE;
}
